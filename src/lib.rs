use worker::*;
use pasetors::keys::{AsymmetricSecretKey, AsymmetricPublicKey, AsymmetricKeyPair};
use pasetors::claims::Claims;
use pasetors::{public, version4::V4};
use pasetors::paserk::{FormatAsPaserk, Id};
use std::convert::TryFrom;
use pasetors::footer::Footer;
use crate::utils::PubKeyListItem;

mod utils;

fn log_request(req: &Request) {
    console_log!(
        "{} - [{}], located at: {:?}, within: {}",
        Date::now().to_string(),
        req.path(),
        req.cf().coordinates().unwrap_or_default(),
        req.cf().region().unwrap_or("unknown region".into())
    );
}

fn get_keypair(ctx: RouteContext<()>) -> Result<AsymmetricKeyPair<V4>> {
    let secret_key = ctx.secret("SIG_KEY")?;
    let secret = AsymmetricSecretKey::<V4>::try_from(secret_key.to_string().as_str()).map_err(|e| {
        Error::RustError(format!("unable to parse secret key: {}", e.to_string()))
    })?;

    let public = AsymmetricPublicKey::<V4>::try_from(&secret).map_err(|e| {
        Error::RustError(format!("unable to generate public key from secret key: {}", e.to_string()))
    })?;

    Ok(AsymmetricKeyPair { public, secret })
}

#[event(fetch)]
pub async fn main(req: Request, env: Env, _ctx: worker::Context) -> Result<Response> {
    log_request(&req);

    // Optionally, get more helpful error messages written to the console in the case of a panic.
    utils::set_panic_hook();

    // Optionally, use the Router to handle matching endpoints, use ":name" placeholders, or "*name"
    // catch-alls to match on specific patterns. Alternatively, use `Router::with_data(D)` to
    // provide arbitrary data that will be accessible in each route via the `ctx.data()` method.
    let router = Router::new();

    // Add as many routes as your Worker needs! Each route will get a `Request` for handling HTTP
    // functionality and a `RouteContext` which you can use to  and get route parameters and
    // Environment bindings like KV Stores, Durable Objects, Secrets, and Variables.
    router
        .get("/", |mut req, ctx| {
            let keys = get_keypair(ctx)?;

            let url = req.url().expect("cannot continue without a URL");
            let mut redirect = match url.query_pairs().find(|(key, value)| {
                key == "url"
            }) {
                Some((_, url)) => match Url::parse(&url) {
                    Ok(u) => u,
                    Err(_) => {
                        return Response::error("unable to parse requested forward url", 400);
                    }
                },
                None => return Response::error("forward url is a required parameter", 400)
            };
            let headers = req.headers();

            let mut claims = Claims::new().map_err(|e| {
                Error::RustError("could not create".to_string())
            })?;

            match headers.get("HTTP_REFERER").unwrap() {
                Some(referrer) => {
                    claims.audience(referrer.as_str());
                }
                None => {}
            };

            match headers.get("CF-Connecting-IP").unwrap() {
                Some(ip) => {
                    claims.subject(ip.as_str());
                }
                None => {}
            }

            match headers.get("CF-RAY").unwrap() {
                Some(ray) => {
                    claims.token_identifier(ray.as_str());
                }
                None => {}
            }

            if url.has_host() {
                claims.issuer(url.host_str().unwrap());
            }

            let mut footer = Footer::new();

            footer.key_id(&Id::from(&keys.public));

            let resp = public::sign(&keys.secret, &claims, Some(&footer), None).map_err(|e| {
                Error::RustError(format!("could not create claims object: {}", e.to_string()))
            })?;

            redirect.query_pairs_mut().append_pair("access_challenge", resp.as_str());

            Response::redirect(redirect)
        })
        .get("/.well-known/paseto/public", |req, ctx| {
            let mut items: Vec<PubKeyListItem> = Vec::new();

            // this could one day become a loop with multiple keys

            let keys = get_keypair(ctx)?;
            let mut item = PubKeyListItem::default();
            keys.public.fmt(&mut item.wpk).map_err(|e| {
                Error::RustError(format!("unable to format paserk: {}", e))
            })?;
            Id::from(&keys.public).fmt(&mut item.kid).map_err(|e| {
                Error::RustError(format!("failed to generate key id: {}", e))
            })?;

            items.push(item);

            Response::from_json(&items)
        })
        .get("/worker-version", |_, ctx| {
            let version = ctx.var("WORKERS_RS_VERSION")?.to_string();
            Response::ok(version)
        })
        .run(req, env)
        .await
}
