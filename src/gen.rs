use pasetors::keys::{AsymmetricKeyPair, Generate};
use pasetors::version4::V4;
use pasetors::paserk::{FormatAsPaserk, Id};
use crate::utils::PubKeyListItem;

mod utils;

fn main() {
    let pair = AsymmetricKeyPair::<V4>::generate().unwrap();

    let mut secret = PubKeyListItem::default();
    pair.secret.fmt(&mut secret.wpk).unwrap();
    Id::from(&pair.secret).fmt(&mut secret.kid).unwrap();

    let mut public = PubKeyListItem::default();
    pair.public.fmt(&mut public.wpk).unwrap();
    Id::from(&pair.public).fmt(&mut public.kid).unwrap();

    print!("{}\n{}",
           serde_json::to_string(&secret).unwrap(),
           serde_json::to_string(&public).unwrap(), );
}